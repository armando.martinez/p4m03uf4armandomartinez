﻿// Sorry porque llegue tan tarde pero mejor tarde que nunca!

using System;

namespace P4M03Uf4ArmandoMartinez
{
    public interface IOrdenable
    {
        int Comparar(IOrdenable x);
    }
    public class ProvaOrdenar
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Data[] dates = new Data[]
                {
                    new Data(6, 1, 2021),
                    new Data(11, 9, 2001),
                    new Data(17, 8, 2017),
                    new Data(11, 3, 2004),
                    new Data(8, 7, 2022),
                    new Data(20, 12, 1973),
                    new Data(13, 11, 2015)
                };

            Ordenar(dates);

            FiguraGeometrica[] figures = new FiguraGeometrica[]
            {
                new Triangle(12, "Quart", ConsoleColor.Yellow, 4, 4),
                new Cercle(1, "Primer", ConsoleColor.DarkBlue, 3),
                new Cercle(8, "Tercer", ConsoleColor.DarkGreen, 5),
                new Rectangle(4, "Segon", ConsoleColor.Green, 3, 4)
            };

            Ordenar(figures);

            Console.WriteLine("No he posat una manera de visualitzar les llistes, pero si com a mínim n'hi ha un breakpoint");
            Console.ReadLine();
        }

        static public void Ordenar(IOrdenable[] list)
        {
            bool ordered;
            do
            {
                ordered = true;
                for (int i = 1; i < list.Length; i++)
                {
                    if (list[i - 1].Comparar(list[i]) != 1 && list[i - 1].Comparar(list[i]) != 0)
                    {
                        ordered = false;
                        IOrdenable auxiliar = list[i];
                        list[i] = list[i - 1];
                        list[i - 1] = auxiliar;
                    }
                }
            } while (!ordered);
        }
    }
}
